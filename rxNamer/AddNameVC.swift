//
//  AddNameVC.swift
//  rxNamer
//
//  Created by javad faghih on 6/23/1399 AP.
//  Copyright © 1399 javad faghih. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift


class AddNameVC: UIViewController {
    
    @IBOutlet weak var newNameTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    let nameSubject = PublishSubject<String>()
    let disposeBage = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindSubmitBtn()
    }
    
    func bindSubmitBtn() {
        
        submitBtn.rx.tap.subscribe(onNext: {
            if self.newNameTextField.text != "" {
                self.nameSubject.onNext(self.newNameTextField.text!)
                
            }
            }).addDisposableTo(disposeBage)
        
        
    }
    

}
