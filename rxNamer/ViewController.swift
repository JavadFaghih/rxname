//
//  ViewController.swift
//  rxNamer
//
//  Created by javad faghih on 6/22/1399 AP.
//  Copyright © 1399 javad faghih. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa



class ViewController: UIViewController {

    @IBOutlet weak var helloLbl: UILabel!
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var namesLbl: UILabel!
    @IBOutlet weak var addNameBtn: UIButton!
    
    let disposeBage = DisposeBag()
    var namesArray: Variable<[String]> = Variable([])
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bindTextField()
        bindSubmitBtn()
        bindAddNameBtn()
        namesArray.asObservable().subscribe(onNext: { names in
            self.namesLbl.text = names.joined(separator: ", ")
            }).addDisposableTo(disposeBage)
    }
    
    func bindTextField() {
        txtField.rx.text
            .debounce(.seconds(1), scheduler: MainScheduler.instance)
            .map {
        
            if $0 == "" {
                return "Type your name below"
                
            } else {
                return  "Hello \($0!)"
            }
            
           
            
        }
        .bind(to: helloLbl.rx.text).addDisposableTo(disposeBage)
        
    }
    
    func bindSubmitBtn() {
        
        submitBtn.rx.tap.subscribe(onNext: {
            if self.txtField.text != "" {
                self.namesArray.value.append(self.txtField.text!)
                self.namesLbl.rx.text.onNext(self.namesArray.value.joined(separator: ", "))
                self.txtField.rx.text.onNext("")
                self.helloLbl.rx.text.onNext("Type your name below")
            }
            }).addDisposableTo(disposeBage)
        
        
    }
    
    func bindAddNameBtn() {
        addNameBtn.rx.tap.throttle(0.5, scheduler: MainScheduler.instance).subscribe(onNext: {
            guard   let vc = self.storyboard?.instantiateViewController(identifier: "addNameVC")  as? AddNameVC else { return }
            vc.nameSubject.subscribe(onNext: { name in
                self.namesArray.value.append(name)
                self.dismiss(animated: true, completion: nil)

            }).addDisposableTo(self.disposeBage)
            self.present(vc, animated: true, completion: nil)
            
        })
        
        
    }
    
  
}

